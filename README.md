# Supplementary Information: On the  Role of Dielectric Screening in Calculating Excited States of Solvated Azobenzene: A Benchmark Study Comparing Quantum Embedding and Polarizable Continuum Model for Representing the Solvent


## Chandrima Chakravarty, Huseyin Aksu, Jessica A. Martinez, Pablo Ramos, Michele Pavanello, and Barry D. Dunietz

### EP_cis.tar.gz:
Embedding potentials for CIS geometry (.pp format and .txt (spline value on a custom grid))

### EP_tras.tar.gz:
Embedding potentials for TRANS geometry ((.pp format and .txt (spline value on a custom grid))

### MIC_20RSN.ipynb:
Jupyter Notebook to select 21 snapshots of the AIMD and get the Minimum image convention.

### RS_cis.xyz/snapshots_ALL_CIS.xyz:
Equilibrated AIMD coordinates for CIS geometry

### RS_trans.xyz/snapshots_ALL_TRANS.xyz:
Equilibrated AIMD coordinates for TRANS geometry

### snapshots_cis.xyz:
Sample snapshots for CIS

### snapshots_trans.xyz:
Sample snapshots for TRANS
